angular.module('appServices',[])
    .factory('getGraphJson', function($http){
            return $http({method: 'GET', url: '/api/get-graph-data'});
    })
    .factory('setGraphJson', function($http){
       return {
           postData : function(data){
               function clone(obj){
                   if(obj == null || typeof(obj) != 'object')
                       return obj;
                   var temp = new obj.constructor();
                   for(var key in obj)
                       temp[key] = clone(obj[key]);
                   return temp;
               }

             //перед отправкрй нужно преобразовать json, в ребрах заменить ноуд, с  объекта этой ноды целиком на ее индекс
             //  делаем через функцию клонирования, ато задевает текущую конфигурацию графа
               var dataReady = clone(data);
               for (var i=0; i < dataReady.links.length;i++){
                   dataReady.links[i].target = dataReady.links[i].target.index;
                   dataReady.links[i].source = dataReady.links[i].source.index;
               }
                         $http({method: 'POST', data: dataReady, url: '/api/set-graph-data'});
                        }
       }
    });