angular.module('app',['appDirectives', 'appServices','ngAnimate'])
    .controller('mainController', function($scope, setGraphJson, getGraphJson, $filter){
        $scope.editorData = {
            link: {},
            nodeToRemove: null,
            linkToRemove: {},
            removeAllNodes: null,
            removeAllLinks: null
        };
        $scope.graphToRender = 1;
        $scope.searchParamsData = {
            nodeToFindShortestPathsFor : null,
            nodeToGetDataSendingEmulationTableFor : null,
            messageLength: null
        };
        $scope.showEditMenu = false;
        $scope.resultPopupData = {
            open: false,
            save: function(){
                $scope.$broadcast('saveNewGraphConfiguration', {
                    data: $scope.resultPopupData.data
                });
                $scope.resultPopupData.open = false;
            }
        };

        $scope.$on('resultToView', function (event, data) {
          switch (data.type){
              case 'routesTable':
                  $scope.resultPopupData.type = 'routesTable';
                  $scope.resultPopupData.data = data.result.searchTable;
                  $scope.resultPopupData.routes = data.result.routesArray;
                  $scope.resultPopupData.nodeNumber = data.nodeNumber;
                  $scope.resultPopupData.open = true;
                  break;
              case 'changeLinksTypeTable':
                  $scope.resultPopupData.type = 'changeLinksTypeTable';
                  $scope.resultPopupData.data = data.result;
                  $scope.resultPopupData.open = true;
                  break;
              case 'dataSendingEmulationTable':
                  $scope.resultPopupData.type ='dataSendingEmulationTable';
                  $scope.resultPopupData.data = data.result;
                  $scope.resultPopupData.nodeNumber = data.nodeNumber;
                  $scope.resultPopupData.messageLength = data.result[0].messageLength;
                  $scope.resultPopupData.maxPackageLength = data.result[0].maxPackageLength;
                  $scope.resultPopupData.open = true;
          }
        });
        $scope.renderGraph = function(){
            $scope.$broadcast('renderGraph', {
                graphType: $scope.graphToRender // посылайте что хотите
            });
        };
        $scope.saveTemporaryGraphToFile = function(){
            $scope.$broadcast('saveTemporaryGraphToFile', {
                graphType: $scope.graphToRender // посылайте что хотите
            });
        };
        $scope.startEditingMode = function(){
            $scope.showEditMenu = !$scope.showEditMenu;
            // запускаем событие вниз
            $scope.$broadcast('startEditingMode', {
                someProp: 'Sending you an Object!' // посылайте что хотите
            });
        };

        $scope.searchClosestPath = function(){
            if (! isNaN(parseInt($scope.searchParamsData.nodeToFindShortestPathsFor))){
                $scope.$broadcast('searchClosestPath', {
                    nodeNumber: $scope.searchParamsData.nodeToFindShortestPathsFor
                });
            }
            $scope.searchParamsData.nodeToFindShortestPathsFor = null;
        };
        $scope.getDataSendingEmulationTable = function(){
            if (! isNaN(parseInt($scope.searchParamsData.nodeToGetDataSendingEmulationTableFor))){
                $scope.$broadcast('dataSendingEmulationTable', {
                    nodeNumber: $scope.searchParamsData.nodeToGetDataSendingEmulationTableFor,
                    messageLength: $scope.searchParamsData.messageLength
                });
            }
            $scope.searchParamsData.nodeToGetDataSendingEmulationTableFor = null;
            $scope.searchParamsData.messageLength = null;
        };

        $scope.addNode = function(){
            // запускаем событие вниз
            $scope.$broadcast('addNode', {});
        };

        $scope.addLink = function(){
            // запускаем событие вниз
            $scope.$broadcast('addLink', {
                link: $scope.editorData.link
            });
            $scope.editorData.link = {};
        };

        $scope.removeNode = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeNode', {
                id: $scope.editorData.nodeToRemove
            });
            $scope.editorData.nodeToRemove = null;
        };

        $scope.removeLink = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeLink', {
                link: $scope.editorData.linkToRemove
            });
            $scope.editorData.linkToRemove = {};
        };

        $scope.removeAllLinks = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeAllLinks', {});
            $scope.editorData.removeAllLinks = null;
        };

        $scope.removeAllNodes = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeAllNodes', {});
            $scope.editorData.removeAllNodes = null;
        };

        $scope.changeLinksType = function(){
            $scope.$broadcast('changeLinksType', {});
        }


    })
    .filter('calculateLinkWeight', function(){
        return function(weight, ifDuplex, ifSatelliteChannel){
            var newLinkWeight = weight;
            if (ifDuplex != 'duplex'){
                newLinkWeight *= 2;
            }
            if (ifSatelliteChannel){
                newLinkWeight *= 3;
            }
            return newLinkWeight;
        }
    })
    .filter('printRoute', function(){
        return function(input){
         if (input){
             var result = "";
             input.forEach(function(item, i){
                 result += (i < input.length-1)?item + "->":item;
             });
             return result;
         }else{
             return "-";
         }
        }
    });