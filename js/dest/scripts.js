angular.module('appDirectives',['appServices'])
    .directive('canvasDirective', function(getGraphJson, setGraphJson, $filter){
        return{
            restrict: 'E',
            replace: true,
            link: function($scope, element, attrs){
                    var APP = function(){
                        //Заполняется с файла
                        this.dataSet = [];
                        //при рассчтеах надо учитывать что 100 байти из каждого значения отводится под заголовки - надо отнимать
                        this.networkVariables = {
                            maxPackageSize : 4000,
                            messageLength : 2000,
                            packageLengthArr : [1800, 2300, 3300],
                            linksPossibleValuesArray: [2, 4, 6, 7, 8, 10, 11, 12, 15, 18, 20, 21, 24]
                        };
                        this.variables = {
                            //режим редактирования графа
                            editMode : true,
                            // используюется так же при построении сетки в режиме редактирования
                            nodeRadius: 7   ,
                            canvas : null,
                            //переменная ссылка на объект графа d3
                            graphForceLayout: null,
                            documentHeight : document.body.clientHeight,
                            documentWidth : document.getElementsByClassName('canvas-wrapper')[0].clientWidth
                        };
                        this.getRandomValue = function(min, max) {
                            var rand = min - 0.5 + Math.random() * (max - min + 1)
                            rand = Math.round(rand);
                            return rand;
                        };
                        this.getRandomPossibleLinkWeight = function(){
                            var min = 0,
                                max = this.networkVariables.linksPossibleValuesArray.length-1;
                            var rand = min + Math.random() * (max + 1 - min);
                            rand = Math.floor(rand);
                            return this.networkVariables.linksPossibleValuesArray[rand];
                        };

                        this.drawGrid = function(){
                            var lineAnimateDuratin = 1000,
                                thisObj = this;
                            var documentDimensions = {
                                    height: this.variables.documentHeight,
                                    width: this.variables.documentWidth
                                },
                                gridStep = this.variables.nodeRadius*2,
                                gridDimensions = {
                                    rows: Math.round(this.variables.documentHeight/gridStep),
                                    cols: Math.round(this.variables.documentWidth/gridStep)
                                },
                                gridRowsDataSet = [],
                                gridColsDataSet = [] ;

                            var fillGridDataSet = function(step){
                                gridRowsDataSet.push(step);
                                return (step  < gridDimensions.rows * gridStep)? fillGridDataSet(step + gridStep):gridRowsDataSet
                            };
                            var fillColsDataSet = function(step){
                                gridColsDataSet.push(step);
                                return (step  < gridDimensions.cols * gridStep)? fillColsDataSet(step + gridStep):gridColsDataSet
                            };
                            fillGridDataSet(0);
                            fillColsDataSet(0);

                            var rows = this.variables.canvas.selectAll('.grid-row').data(gridRowsDataSet).enter().append('line');
                            var cols = this.variables.canvas.selectAll('.grid-col').data(gridColsDataSet).enter().append('line');


                            if (this.variables.editMode){
                                rows
                                    .attr('class','grid-row')
                                    .attr("x1", function(d){ return 0; })
                                    .attr("y1", function(d){ return d + gridStep; })
                                    //дополнительно для анимации
                                    .attr("x2", function(d){ return 0; })
                                    .attr("y2", function(d){ return d + gridStep; })
                                    .transition()
                                    .duration(lineAnimateDuratin)
                                    .attr("x2", function(d){ return documentDimensions.width })
                                    .attr("y2", function(d){ return d + gridStep })


                                cols
                                    .attr('class','grid-col')
                                    .attr("x1", function(d){ return d + gridStep; })
                                    .attr("y1", function(d){ return 0; })
                                    //дополнительно для анимации
                                    .attr("x2", function(d){ return d + gridStep; })
                                    .attr("y2", function(d){ return 0; })
                                    .transition()
                                    .duration(lineAnimateDuratin)
                                    .attr("x2", function(d){ return d + gridStep })
                                    .attr("y2", function(d){ return documentDimensions.height })
                            }else{

                                //    отключаем
                                this.variables.canvas.selectAll('.grid-row')
                                    .transition()
                                    .duration(lineAnimateDuratin)
                                    .attr("x1", function(d){ return 0; })
                                    .attr("y1", function(d){ return d - gridStep; })
                                    .attr("x2", function(d){ return 0 })
                                    .attr("y2", function(d){ return d - gridStep })
                                //.data([]).exit().remove();

                                this.variables.canvas.selectAll('.grid-col')

                                    .transition()
                                    .duration(lineAnimateDuratin)
                                    .attr("y1", function(d){ return d - gridStep; })
                                    .attr("y1", function(d){ return 0; })
                                    .attr("y2", function(d){ return d - gridStep })
                                    .attr("y2", function(d){ return 0 })
                                //.data([]).exit().remove();

                                setTimeout(function(){
                                    thisObj.variables.canvas.selectAll('.grid-row').remove();
                                    thisObj.variables.canvas.selectAll('.grid-col').remove();
                                }, lineAnimateDuratin);




                            }
                        }

                        //вспомогательные функции для крада
                        this.findNode = function(id) {
                            for (var i in this.dataSet.nodes) {
                                if (this.dataSet.nodes[i]["index"] === id) return this.dataSet.nodes[i];};

                        };

                        this.findNodeIndex = function(id) {
                            for (var i=0;i<this.dataSet.nodes.length;i++) {
                                if (this.dataSet.nodes[i].index==id){
                                    return i;
                                }
                            }
                        };

                        // Input param
                        // object {
                        // id: id
                        // name: name
                        // fixed: true | false
                        // }
                        this.addNode = function(object){
                            this.dataSet.nodes.push({
                                //TODO ?????????????
                                //"index": this.dataSet.nodes.length,
                                "index": (this.dataSet.nodes.length > 0)?this.dataSet.nodes[this.dataSet.nodes.length - 1].index + 1:0,
                                "px": object.x,
                                "py": object.y,
                                "name": object.name,
                                "fixed": object.fixed || ""
                            });
                            this.renderGraph();
                        };

                        //принимает айди нужной ноды
                        this.removeNode = function (id) {
                            var i = 0;
                            var n = this.findNode(id);
                            while (i < this.dataSet.links.length) {
                                if ((this.dataSet.links[i]['source'] == n)||(this.dataSet.links[i]['target'] == n))
                                {
                                    this.dataSet.links.splice(i,1);
                                }
                                else i++;
                            }
                            this.dataSet.nodes.splice(this.findNodeIndex(id),1);
                            this.renderGraph();
                        };

                        //принимае 2 параметра айди начала, ребра и айди конца
                        this.removeLink = function (source, target) {
                            var i = 0;
                            while (i < this.dataSet.links.length) {
                                if ((this.dataSet.links[i]['source'].index == source) && (this.dataSet.links[i]['target'].index == target) ||
                                    (this.dataSet.links[i]['source'].index == target) && (this.dataSet.links[i]['target'].index == source) )
                                {
                                    this.dataSet.links.splice(i,1);
                                }
                                else i++;
                            }
                            this.renderGraph();
                        };

                        //тут когда пушим линку надо вставить в соурс и таргет не айдишник, а саму ноду. так делает d3
                        this.addLink = function (source, target, value) {
                            if (! value)
                                value = this.getRandomPossibleLinkWeight();
                            console.log(this.dataSet);
                            this.dataSet.links.push({"index": this.dataSet.links.length + 1,"source":this.findNode(source),"target":this.findNode(target),"fixed":true, "weight":value, "type":"duplex", "satelliteChannel": false});
                            this.renderGraph();
                        };

                        this.removeAllLinks = function(){
                            this.dataSet.links.splice(0,this.dataSet.links.length);
                            //обнуляем матрицу смежности
                            this.dataSet.graphMatrix = [];
                            this.renderGraph();
                        };

                        this.removeAllNodes = function(){
                            this.dataSet.links.splice(0,this.dataSet.links.length);
                            this.dataSet.nodes.splice(0,this.dataSet.nodes.length);
                            //обнуляем матрицу смежности
                            this.dataSet.graphMatrix = [];
                            this.renderGraph();
                        };


                        //Force layout - представление данных в виде графа из либы d3
                        this.renderGraph = function(){
                            console.log(this.dataSet.links);


                            var link = this.variables.canvas.selectAll("g.line")
                                .data(this.dataSet.links, function(d) {
                                    //TODO index пришлось добавить что бы было можно удалять ребра ( как сделать по другому?). КАждому ребру при инициализации в data() указываем коллбеком индекс
                                    return d.index;
                                });
                            var linkEnter = link.enter().append("g")
                                .attr("class", "line")

                                .call(this.variables.graphForceLayout.drag);
                            linkEnter.append("svg:line")
                                .attr("class", function(d){
                                    var classList = '';
                                    if (d.type == 'duplex'){
                                       classList+=  ' line duplex';
                                    }else{
                                        classList += ' line half-duplex';
                                    }
                                    if (d.satelliteChannel){
                                        classList += ' satelliteChannel';
                                    }
                                    return classList;
                                })
                                .attr("x1", function(d){ return d.source.px;})
                                .attr("y1", function(d){ return d.source.py;})
                                .attr("x2", function(d){ return d.target.px;})
                                .attr("y2", function(d){ return d.target.py;});
                            linkEnter.append("svg:text")
                                .attr('class','edgeName')
                                .attr('x', function(d){
                                    //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                                    return Math.round((d.source.px + d.target.px )/2)
                                })
                                .attr('y', function(d){
                                    //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                                    return Math.round((d.source.py + d.target.py )/2)
                                })
                                .text(function(d){
                                   return $filter('calculateLinkWeight')(d.weight, d.type, d.satelliteChannel);
                                });
                            link.exit().remove();

                            var node = this.variables.canvas.selectAll("g.node")
                                .data(this.dataSet.nodes, function(d) {
                                    return d.index;});
                            var nodeEnter = node.enter().append("g")
                                .attr("class", "node")
                                .call(this.variables.graphForceLayout.drag);
                            nodeEnter.append("svg:circle")
                                .attr("r", this.variables.nodeRadius)
                                .attr("cx", function(d){return d.px})
                                .attr("cy", function(d){return d.py})
                                .attr("class","circle");
                            nodeEnter.append("svg:text")
                                .attr("class","nodeName")
                                .text(function(d){return d.index});
                            node.exit().remove();

                            this.variables.graphForceLayout.on("tick", tick);
                            var t = this;
                            function tick(e) {
                                link.selectAll('line')
                                    .attr("x1", function(d){ return d.source.px; })
                                    .attr("y1", function(d){ return d.source.py; })
                                    .attr("x2", function(d){ return d.target.px; })
                                    .attr("y2", function(d){ return d.target.py; });
                                link.select('text')
                                    .attr('x', function(d){
                                        //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                                        return Math.round((d.source.px + d.target.px )/2)
                                    })
                                    .attr('y', function(d){
                                        //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                                        return Math.round((d.source.py + d.target.py )/2)
                                    });
                                node.selectAll('circle')
                                    .attr("cx", function(d) { return d.px; })
                                    .attr("cy", function(d) { return d.py; });
                                node.select('text')
                                    .attr("x", function(d){return (d.px +  20);})
                                    .attr("y", function(d){ return d.py - 5 ;});
                            }

                            console.log(this.dataSet);

                            //при перерисовке графа обновляем матрицу смежности
                            this.generateAdjacencyMatrix();

                        };

                        //Возвращает сколько у ноды исходящих/входящих ребер. Используем для генерации графа по заданой тополошгии
                        this.getNodeOucomingEdges = function(nodeId){
                            var sum = 0;
                            this.dataSet.graphMatrix[nodeId].map(function(item){
                                (item > 0)? sum+=1:sum+=0;
                            });
                            return sum;
                        };
                        //Функия для проверки остались ли у нас еще ноды в которые мы согласно алгоритму можем добавить ребро
                        this.checkOutcomingEdges = function(){
                            var resArr = [];
                            for (var i=0; i< this.dataSet.graphMatrix.length;i++){
                                var sum=0;
                                this.dataSet.graphMatrix[i].map(function(item){
                                    (item > 0)? sum+=1:sum+=0;
                                });
                                resArr.push(sum);
                            }
                            //alert(Math.min.apply(null, resArr));
                            return Math.min.apply(null, resArr);
                        };
                        this.generateRandomGraph = function(){
                                var k = 0, maxSubnet = 25, netRadius = 300;
                            var vertexEdgesIntensity = 5;
                                for (var i = 0; i < maxSubnet; i++){
                                    var x  =  this.variables.documentWidth / 2 + netRadius * Math.cos(2 * Math.PI * i / maxSubnet);
                                    var y =  this.variables.documentHeight / 2 + netRadius * Math.sin(2 * Math.PI * i / maxSubnet);
                                    //var x = this.getRandomValue(20, this.variables.documentWidth - 20);
                                    //var y = this.getRandomValue(20, this.variables.documentHeight - 20);
                                    this.addNode({
                                        "x":x,
                                        "y":y,
                                        name:"",
                                        fixed: true
                                    });
                                }
                            //tempNode - узел для которго происходит поиска напарника для составления ребра. Проверяем что бы не было цыклов
                            var getRandomFreeNode = function(min, max, tempNode){
                                var num = Math.floor(Math.random() * (max - min + 1)) + min;
                                //alert(min + ' ' + max + ' ' + tempNode + ' result' + num);

                                return (tempNode == num ) ? getRandomFreeNode(min, max, tempNode) : num;
                            };
                            for(var j=0; j<this.dataSet.nodes.length; j++ ){
                                //var nodeOucomingLinks = ;
                                while(this.getNodeOucomingEdges(j) < vertexEdgesIntensity){
                                    //nodeOucomingLinks = this.getNodeOucomingEdges(j);
                                    //TODO ТУТ ВАЖНО maxSubnet - 1 !!!!!!. Ато всего узлов 25 например, но нумерация с 0!!!!
                                    var nodePartner = getRandomFreeNode(0,maxSubnet-1, j);
                                    if (this.checkOutcomingEdges() >= vertexEdgesIntensity){
                                        if (this.getNodeOucomingEdges(nodePartner) <5 && this.dataSet.graphMatrix[j][nodePartner] == 0){
                                            this.addLink(j,nodePartner, this.getRandomPossibleLinkWeight());
                                        }
                                    }else{
                                        if (this.dataSet.graphMatrix[j][nodePartner] == 0){
                                            this.addLink(j,nodePartner, this.getRandomPossibleLinkWeight());
                                        }
                                    }
                                }
                            }
                        };
                        this.init = function(){
                            var force = d3.layout.force()
                                .distance(400)
                                .charge(-50)
                                .size([this.variables.documentWidth, this.variables.documentHeight])
                                .nodes(this.dataSet.nodes)
                                .links(this.dataSet.links)
                                .start();

                            this.variables.graphForceLayout = force;
                            // только после того как определеили все параметри нод можем запускать инициализацию лейаута
                            //this.renderGraph();
                        };

                        this.startEditMode = function(){
                            if (!this.variables.editMode) {
                                setGraphJson.postData(this.dataSet);
                                this.clearRenderedGraphInView();
                                this.renderGraph();
                            }
                                this.variables.editMode = ! this.variables.editMode;
                        };

                        //для дебага
                        this.consoleLogMatrix = function(matrix){
                            for (var i=0; i < matrix.length; i++){
                                console.log(matrix[i].toString());
                            }
                        };
                        // заполняет матрицу смежностти графа, на основе данных с d3
                        this.generateAdjacencyMatrix = function(){
                            //инициализируем массив
                            for (var i = 0; i < this.dataSet.nodes.length; i++){
                                this.dataSet.graphMatrix[i] = [];
                                for (var j = 0 ; j < this.dataSet.nodes.length; j++){
                                    this.dataSet.graphMatrix[i][j] = 0;
                                }
                            }
                            //устанавливаем вес кажого из ребер в нужную ячейку
                            for (var k = 0; k  < this.dataSet.nodes.length ; k ++){
                                for (var l = 0; l < this.dataSet.links.length ; l++){
                                    //console.log(k);
                                    var graphMatrixIndex = this.dataSet.links[l].source.index;
                                    if (this.dataSet.links[l].source.index == k){
                                        var linkWeight =  $filter('calculateLinkWeight')(this.dataSet.links[l].weight, this.dataSet.links[l].type, this.dataSet.links[l].satelliteChannel);
                                        this.dataSet.graphMatrix[graphMatrixIndex][this.dataSet.links[l].target.index] = linkWeight;
                                        this.dataSet.graphMatrix[this.dataSet.links[l].target.index][graphMatrixIndex] = linkWeight;
                                    }
                                }
                            }
                            this.consoleLogMatrix(this.dataSet.graphMatrix);
                        };

                        //Функция поиска кратчайшего пути от заданой вершины к остальным вершинам графа (маршрутизация) по алгоритму Форда-Беллмана
                        // start - вершина для которой производится поиск
                        // nodesCount - колличство вершин графа
                        // matrix - матрица смежности графа
                        this.searchShortestPathes = function(start, nodesCount, matrix){
                            //готовим массив к поиску TODO нужно ли это?
                            //    var array = this.dataSet.nodes;
                            //    var newVertex = array.splice(start,1);
                            //    array.unshift(newVertex[0]);
                            var array = this.dataSet.nodes;
                            var searchTable = [];
                            // тут храним текущею и предидущую вершиныЮ для исключения их с проверки
                            var  vertexArray = [];

                            //тут собитраем предков что бы восстановить путь
                            var parentsArray = [];
                            //финальный массив в котором будут пути для каждо из точек
                            var routesArray = [];
                            // сначала инициализируем таблицу поиска (всем кроме стартовой вершины - infinity)
                            for (var k =0; k < nodesCount; k ++){
                                searchTable[k] = Infinity;
                                parentsArray[k] = [];
                            }
                            // вершину начала поиска обнуляем
                            searchTable[start] = 0;
                            vertexArray.push(start);
                            // n-1 итераций согласно алгоритму
                            console.log("INITTTTTTT ARRAY");
                            console.log(parentsArray);
                            for(var m=0; m < array.length -1 ; m++){
                                for (var i=0; i < array.length; i++){
                                    vertexArray.push(array[i].index);
                                    var prevVertex =vertexArray[0];
                                    var tempVertex =vertexArray[vertexArray.length-1];
                                    //console.log("prev vertex" + vertexArray[0]);
                                    //console.log("temp vertex" + tempVertex);
                                    console.log("TABLE" + searchTable);
                                    console.log(searchTable);
                                    for (var j=0; j < array.length; j++){
                                        if (matrix[tempVertex][j] > 0 &&  matrix[tempVertex][j] + searchTable[tempVertex] < searchTable[j]){
                                            searchTable[j] = matrix[tempVertex][j] + searchTable[tempVertex];
                                            parentsArray[j] = tempVertex;
                                        }
                                    }
                                    //console.log(searchTable);
                                    //console.log("===================");
                                    vertexArray.shift();
                                }
                            };

                            //Теперь соберем новый массив, на основе parentsArray, в который запишем сколько узлов нужно
                            //прости сообщению с исходной точки до остальных и восстановим путь
                            for (var n=0; n<array.length; n++){
                                if (n != start){
                                    routesArray[n] = ({
                                        'route': [],
                                        'nodesCount': null
                                    });
                                    var prevNode = parentsArray[n];
                                    routesArray[n].route.push(n);
                                    while (prevNode != start){
                                        routesArray[n].route.push(prevNode);
                                        prevNode =parentsArray[prevNode]
                                    }
                                    //Заканчиваем путь искходой вершиной
                                    routesArray[n].route.push(parseInt(start));
                                    //Переворачиваем массив, так как восстанавливали путь с финишной точки пути
                                    routesArray[n].route.reverse();
                                    //Добавляем колличество узлов в маршруте
                                    routesArray[n].nodesCount = routesArray[n].route.length;
                                }
                            }

                            return {
                                'searchTable': searchTable,
                                'routesArray': routesArray
                            };
                        };

                        //Функция времени передачи ппкета от источника в другие точки сети
                        //start - вершина для которой производится поиск
                        // nodesCount - колличство вершин графа
                        this.dataSendingEmulationTable = function(start, messageLengthParam, nodesCount){
                            var resultData = [],
                                shortestPathsTable = this.searchShortestPathes(start, nodesCount, this.dataSet.graphMatrix).searchTable,
                                routesArray = this.searchShortestPathes(start, nodesCount, this.dataSet.graphMatrix).routesArray,
                                messageLength = messageLengthParam || this.getRandomValue(1500,5000),
                                sendingTypes = [
                                    {
                                        "name": "Лог. з'єднання",
                                        "delay": 4,
                                        "minServicePackageCount": 6
                                    },
                                    {
                                        "name": "Дейт. з'єднання",
                                        "delay": 0,
                                        "minServicePackageCount": 2
                                    }
                                ];
                            resultData.push({
                               'messageLength': messageLength,
                                'maxPackageLength': this.networkVariables.maxPackageSize
                            });
                            //Добавляем вероятность появления лишних пакетов
                            function randomPackageProbability() {
                                var notRandomNumbers = [0,0,0,0,0,1,1,1, 2,2, 3, 4, 5];
                                var idx = Math.floor(Math.random() * notRandomNumbers.length);
                                return notRandomNumbers[idx];
                            }

                            //Прогоняем для логического соединения и дейтаграм
                            for (var k=0; k<sendingTypes.length; k++){
                                var sendingTypeName = sendingTypes[k].name;
                                var sendingTypeDelay = sendingTypes[k].delay;
                                var minServicePackageCount = sendingTypes[k].minServicePackageCount;
                                for (var i=0; i < this.networkVariables.packageLengthArr.length; i++){
                                    resultData.push({
                                        "name":  sendingTypeName,
                                        "packageLength": this.networkVariables.packageLengthArr[i],
                                        "children": []
                                    });
                                    for (var j=0; j< nodesCount; j++){
                                        if (j != start){
                                            var packageNum = Math.ceil(messageLength / (this.networkVariables.packageLengthArr[i] - 100));
                                            var result =  packageNum * shortestPathsTable[j] + sendingTypeDelay*packageNum;
                                            var nodesConnectionsCount = routesArray[j].nodesCount - 1;
                                            var nodesConnectionNotFull = nodesConnectionsCount - 2;
                                            var servicePackages = 0;
                                            if (minServicePackageCount == "6"){
                                                if (nodesConnectionsCount+1 != 2){
                                                    servicePackages = minServicePackageCount /2 * nodesConnectionNotFull * packageNum  + minServicePackageCount * 2;
                                                    if(nodesConnectionsCount+1 == 3){
                                                        servicePackages =  minServicePackageCount * 2;
                                                    }
                                                }else{

                                                        servicePackages =  minServicePackageCount ;

                                                }
                                            }else{
                                                servicePackages = nodesConnectionsCount * minServicePackageCount;
                                            }
                                            alert(servicePackages);
                                            resultData[resultData.length - 1].children.push({
                                                "nodeIndex": j,
                                                "value": result,
                                                "packages": {
                                                    "information": packageNum * nodesConnectionsCount,
                                                    "service": servicePackages
                                                }
                                            });

                                        }
                                    }
                                }
                            }
                            return resultData;
                        };

                        this.chooseGraphType = function(type){
                            var self = this;
                            this.clearRenderedGraphInView();
                            // только после того как определеили все параметри нод можем запускать инициализацию лейаута
                            if (type == 1){
                                getGraphJson.success(function(data, status, headers, config) {
                                    self.dataSet = data;
                                    self.init();
                                    //self.init();
                                    self.renderGraph();
                                });
                            }
                            if(type == 2){
                                this.init();
                                this.generateRandomGraph();
                                this.renderGraph();
                            }
                        };
                        //Функция для очисти отрисованого графа, что бы было видно внесенные в сеть  изменения
                        this.clearRenderedGraphInView = function(){
                            this.dataSet = {"graphMatrix":[],"nodes":[],"links":[]};
                            this.init();
                            this.renderGraph();

                        }
                    };


                    var app = new APP();
                    app.dataSet = {"graphMatrix":[],"nodes":[],"links":[]};
                var svg = d3.select('body').select('.canvas-wrapper')
                    .append('svg')
                    .attr('height', app.variables.documentHeight)
                    .attr('width', app.variables.documentWidth);

                app.variables.canvas = svg;
                    app.drawGrid();


                    //TODO слушаем руководящие собыия с главной страницы
                    $scope.$on('startEditingMode', function (event, data) {
                        app.startEditMode();
                    });

                    $scope.$on('renderGraph', function (event, data) {
                        app.chooseGraphType(data.graphType);
                    });


                    $scope.$on('addNode', function (event, data) {
                        app.addNode({
                            "x":50,
                            "y":50,
                            name:"",
                            fixed: true
                        });
                    });

                    $scope.$on('addLink', function (event, data) {
                        app.addLink(parseInt(data.link.source), parseInt(data.link.target), parseInt(data.link.weight));
                    });

                    $scope.$on('removeNode', function (event, data) {
                        app.removeNode(parseInt(data.id));
                    });

                    $scope.$on('removeLink', function (event, data){
                        app.removeLink(parseInt(data.link.source), parseInt(data.link.target));
                    });

                    $scope.$on('removeAllNodes', function (event, data) {
                        app.removeAllNodes();
                    });

                    $scope.$on('removeAllLinks', function (event, data){
                        app.removeAllLinks();
                    });

                    $scope.$on('changeLinksType', function (event, data){
                        $scope.$broadcast('resultToView', {
                            type: 'changeLinksTypeTable',
                            result: app.dataSet
                        });
                    });
                    $scope.$on('saveNewGraphConfiguration', function (event, data) {
                        //Обнуляем Граф !!! перед отрисовкой изменений
                        app.clearRenderedGraphInView();

                        var self = app;
                        app.dataSet = data.data;
                        //setGraphJson.postData(data.data);
                        self.init();
                        app.generateAdjacencyMatrix();

                        self.renderGraph();
                    });
                    $scope.$on('saveTemporaryGraphToFile', function(event, data){
                        alert('Saved');
                        setGraphJson.postData(app.dataSet);
                    });

                    //TODO слушаем руководящие собыия с главной страницы
                    $scope.$on('searchClosestPath', function (event, data) {
                        var nodeNumber = data.nodeNumber;
                        $scope.$broadcast('resultToView', {
                            type: 'routesTable',
                            nodeNumber: nodeNumber,
                            result: app.searchShortestPathes(nodeNumber, app.dataSet.nodes.length, app.dataSet.graphMatrix)
                        });
                    });
                    $scope.$on('dataSendingEmulationTable', function (event, data) {
                        var nodeNumber = data.nodeNumber;
                        var messageLength = data.messageLength;
                        $scope.$broadcast('resultToView', {
                            type: 'dataSendingEmulationTable',
                            nodeNumber: nodeNumber,
                            messageLength: messageLength,
                            result: app.dataSendingEmulationTable(nodeNumber, messageLength, app.dataSet.nodes.length)
                        });
                    });
            }
        }
    })
    .directive('ngDraggable', function($document, $window){
    function makeDraggable(scope, element, attr) {
        var startX = 0;
        var startY = 0;

        // Start with a random pos
        var x = Math.floor(document.body.clientWidth/2 - document.body.clientWidth*0.5);
        var y = Math.floor(document.body.clientHeight/2 - document.body.clientHeight*0.5);

        element.css({
            position: 'absolute',
            cursor: 'pointer',
            top: y + 'px',
            left: x + 'px'
        });

        element.on('mousedown', function(event) {
            event.preventDefault();

            startX = event.pageX - x;
            startY = event.pageY - y;

            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);
        });

        function mousemove(event) {
            y = event.pageY - startY;
            x = event.pageX - startX;

            element.css({
                top: y + 'px',
                left: x + 'px'
            });
        }

        function mouseup() {
            $document.unbind('mousemove', mousemove);
            $document.unbind('mouseup', mouseup);
        }
    }
    return {
        link: makeDraggable
    };
});
angular.module('app',['appDirectives', 'appServices','ngAnimate'])
    .controller('mainController', function($scope, setGraphJson, getGraphJson, $filter){
        $scope.editorData = {
            link: {},
            nodeToRemove: null,
            linkToRemove: {},
            removeAllNodes: null,
            removeAllLinks: null
        };
        $scope.graphToRender = 1;
        $scope.searchParamsData = {
            nodeToFindShortestPathsFor : null,
            nodeToGetDataSendingEmulationTableFor : null,
            messageLength: null
        };
        $scope.showEditMenu = false;
        $scope.resultPopupData = {
            open: false,
            save: function(){
                $scope.$broadcast('saveNewGraphConfiguration', {
                    data: $scope.resultPopupData.data
                });
                $scope.resultPopupData.open = false;
            }
        };

        $scope.$on('resultToView', function (event, data) {
          switch (data.type){
              case 'routesTable':
                  $scope.resultPopupData.type = 'routesTable';
                  $scope.resultPopupData.data = data.result.searchTable;
                  $scope.resultPopupData.routes = data.result.routesArray;
                  $scope.resultPopupData.nodeNumber = data.nodeNumber;
                  $scope.resultPopupData.open = true;
                  break;
              case 'changeLinksTypeTable':
                  $scope.resultPopupData.type = 'changeLinksTypeTable';
                  $scope.resultPopupData.data = data.result;
                  $scope.resultPopupData.open = true;
                  break;
              case 'dataSendingEmulationTable':
                  $scope.resultPopupData.type ='dataSendingEmulationTable';
                  $scope.resultPopupData.data = data.result;
                  $scope.resultPopupData.nodeNumber = data.nodeNumber;
                  $scope.resultPopupData.messageLength = data.result[0].messageLength;
                  $scope.resultPopupData.maxPackageLength = data.result[0].maxPackageLength;
                  $scope.resultPopupData.open = true;
          }
        });
        $scope.renderGraph = function(){
            $scope.$broadcast('renderGraph', {
                graphType: $scope.graphToRender // посылайте что хотите
            });
        };
        $scope.saveTemporaryGraphToFile = function(){
            $scope.$broadcast('saveTemporaryGraphToFile', {
                graphType: $scope.graphToRender // посылайте что хотите
            });
        };
        $scope.startEditingMode = function(){
            $scope.showEditMenu = !$scope.showEditMenu;
            // запускаем событие вниз
            $scope.$broadcast('startEditingMode', {
                someProp: 'Sending you an Object!' // посылайте что хотите
            });
        };

        $scope.searchClosestPath = function(){
            if (! isNaN(parseInt($scope.searchParamsData.nodeToFindShortestPathsFor))){
                $scope.$broadcast('searchClosestPath', {
                    nodeNumber: $scope.searchParamsData.nodeToFindShortestPathsFor
                });
            }
            $scope.searchParamsData.nodeToFindShortestPathsFor = null;
        };
        $scope.getDataSendingEmulationTable = function(){
            if (! isNaN(parseInt($scope.searchParamsData.nodeToGetDataSendingEmulationTableFor))){
                $scope.$broadcast('dataSendingEmulationTable', {
                    nodeNumber: $scope.searchParamsData.nodeToGetDataSendingEmulationTableFor,
                    messageLength: $scope.searchParamsData.messageLength
                });
            }
            $scope.searchParamsData.nodeToGetDataSendingEmulationTableFor = null;
            $scope.searchParamsData.messageLength = null;
        };

        $scope.addNode = function(){
            // запускаем событие вниз
            $scope.$broadcast('addNode', {});
        };

        $scope.addLink = function(){
            // запускаем событие вниз
            $scope.$broadcast('addLink', {
                link: $scope.editorData.link
            });
            $scope.editorData.link = {};
        };

        $scope.removeNode = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeNode', {
                id: $scope.editorData.nodeToRemove
            });
            $scope.editorData.nodeToRemove = null;
        };

        $scope.removeLink = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeLink', {
                link: $scope.editorData.linkToRemove
            });
            $scope.editorData.linkToRemove = {};
        };

        $scope.removeAllLinks = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeAllLinks', {});
            $scope.editorData.removeAllLinks = null;
        };

        $scope.removeAllNodes = function(){
            // запускаем событие вниз
            $scope.$broadcast('removeAllNodes', {});
            $scope.editorData.removeAllNodes = null;
        };

        $scope.changeLinksType = function(){
            $scope.$broadcast('changeLinksType', {});
        }


    })
    .filter('calculateLinkWeight', function(){
        return function(weight, ifDuplex, ifSatelliteChannel){
            var newLinkWeight = weight;
            if (ifDuplex != 'duplex'){
                newLinkWeight *= 2;
            }
            if (ifSatelliteChannel){
                newLinkWeight *= 3;
            }
            return newLinkWeight;
        }
    })
    .filter('printRoute', function(){
        return function(input){
         if (input){
             var result = "";
             input.forEach(function(item, i){
                 result += (i < input.length-1)?item + "->":item;
             });
             return result;
         }else{
             return "-";
         }
        }
    });
angular.module('appServices',[])
    .factory('getGraphJson', function($http){
            return $http({method: 'GET', url: '/api/get-graph-data'});
    })
    .factory('setGraphJson', function($http){
       return {
           postData : function(data){
               function clone(obj){
                   if(obj == null || typeof(obj) != 'object')
                       return obj;
                   var temp = new obj.constructor();
                   for(var key in obj)
                       temp[key] = clone(obj[key]);
                   return temp;
               }

             //перед отправкрй нужно преобразовать json, в ребрах заменить ноуд, с  объекта этой ноды целиком на ее индекс
             //  делаем через функцию клонирования, ато задевает текущую конфигурацию графа
               var dataReady = clone(data);
               for (var i=0; i < dataReady.links.length;i++){
                   dataReady.links[i].target = dataReady.links[i].target.index;
                   dataReady.links[i].source = dataReady.links[i].source.index;
               }
                         $http({method: 'POST', data: dataReady, url: '/api/set-graph-data'});
                        }
       }
    });