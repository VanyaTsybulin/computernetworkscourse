document.addEventListener("DOMContentLoaded", function(event) {
    var APP = function(){
        this.dataSet = {
            // динамически щаполняется с датасета для d3, по сути это матрица смежности, будет использоваться
            // для мат. манипуляция с графом
            graphMatrix : [],
            nodes : [
                {"index" : 0, "x":50, "y":100, "name": "A", "fixed": true},
                {"index" : 1, "x":350, "y":150,"name": "B", "fixed": true},
                {"index" : 2, "x":550, "y":350,"name": "C", "fixed": true},
                {"index" : 3, "x":450, "y":450,"name": "D", "fixed": true},
                {"index" : 4, "x":350, "y":550, "name": "E", "fixed": true},
                {"index" : 5, "x":50, "y":350, "name": "S", "fixed": true}
            ],
            links: [
                //A-B
                {"index" : 0, "source": 0, "target": 1, "visible": true, 'weight':1},
                //B-C
                {"index" : 1, "source": 1, "target": 2, "visible": true, 'weight':2},
                //S-A
                {"index" : 2, "source": 0, "target": 5, "visible": true, 'weight':10},
                //D-E
                {"index" : 3, "source": 3, "target": 4, "visible": true, 'weight':1},
                //C-D
                {"index" : 4, "source": 2, "target": 3, "visible": true, 'weight':1},
                //E-S
                {"index" : 5, "source": 4, "target": 5, "visible": true, 'weight':8},
                //A-D
                {"index" : 6, "source": 0, "target": 3, "visible": true, 'weight':4},
                //A-C
                {"index" : 7, "source": 0, "target": 2, "visible": true, 'weight':2}
            ]
        };
        this.variables = {
            //режим редактирования графа
            editMode : true,

            // используюется так же при построении сетки в режиме редактирования
            nodeRadius: 10,

            canvas : null,
            //переменная ссылка на объект графа d3
            graphForceLayout: null,
            documentHeight : document.body.clientHeight,
            documentWidth : document.body.clientWidth
        };




        //вспомогательные функции для крада
        this.findNode = function(id) {
            for (var i in this.dataSet.nodes) {
                if (this.dataSet.nodes[i]["index"] === id) return this.dataSet.nodes[i];};

        };

        this.findNodeIndex = function(id) {
            for (var i=0;i<this.dataSet.nodes.length;i++) {
                if (this.dataSet.nodes[i].index==id){
                    return i;
                }
            }
        };

        // Input param
        // object {
        // id: id
        // name: name
        // fixed: true | false
        // }
        this.addNode = function(object){
            this.dataSet.nodes.push({
                "index": this.dataSet.nodes.length,
                "px": object.x,
                "py": object.y,
                "name": object.name,
                "fixed": object.fixed || ""
            });
            this.renderGraph();
        };

        //принимает айди нужной ноды
        this.removeNode = function (id) {
            var i = 0;
            var n = this.findNode(id);
            while (i < this.dataSet.links.length) {
                if ((this.dataSet.links[i]['source'] == n)||(this.dataSet.links[i]['target'] == n))
                {
                    this.dataSet.links.splice(i,1);
                }
                else i++;
            }
            this.dataSet.nodes.splice(this.findNodeIndex(id),1);
            this.renderGraph();
        };

        //принимае 2 параметра айди начала, ребра и айди конца
        this.removeLink = function (source, target) {
            var i = 0;
            while (i < this.dataSet.links.length) {
                if ((this.dataSet.links[i]['source'].index == source) && (this.dataSet.links[i]['target'].index == target) ||
                    (this.dataSet.links[i]['source'].index == target) && (this.dataSet.links[i]['target'].index == source) )
                {
                    this.dataSet.links.splice(i,1);
                }
                else i++;
            }
            this.renderGraph();
        };

        //тут когда пушим линку надо вставить в соурс и таргет не айдишник, а саму ноду. так делает d3
        this.addLink = function (source, target, value) {
            this.dataSet.links.push({"index": this.dataSet.links.length + 1,"source":this.findNode(source),"target":this.findNode(target),"fixed":true, "weight":value});
            this.renderGraph();
        };

        this.removeAllLinks = function(){
            this.dataSet.links.splice(0,this.dataSet.links.length);
            //обнуляем матрицу смежности
            this.dataSet.graphMatrix = [];
            this.renderGraph();
        };

        this.removeAllNodes = function(){
            this.dataSet.links.splice(0,this.dataSet.links.length);
            this.dataSet.nodes.splice(0,this.dataSet.nodes.length);
            //обнуляем матрицу смежности
            this.dataSet.graphMatrix = [];
            this.renderGraph();
        };


        //Force layout - представление данных в виде графа из либы d3
        this.renderGraph = function(){

            var link = this.variables.canvas.selectAll("g.line")
                .data(this.dataSet.links, function(d) {
                    //TODO index пришлось добавить что бы было можно удалять ребра ( как сделать по другому?). КАждому ребру при инициализации в data() указываем коллбеком индекс
                    return d.index;
                });
            var linkEnter = link.enter().append("g")
                .attr("class", "line")
                .call(this.variables.graphForceLayout.drag);
            linkEnter.append("svg:line")
                .attr("class","line")
                .attr("x1", function(d){ return d.source.x;})
                .attr("y1", function(d){ return d.source.y;})
                .attr("x2", function(d){ return d.target.x;})
                .attr("y2", function(d){ return d.target.y;});
            linkEnter.append("svg:text")
                .attr('class','edgeName')
                .attr('x', function(d){
                    //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                    return Math.round((d.source.px + d.target.px )/2)
                })
                .attr('y', function(d){
                    //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                    return Math.round((d.source.py + d.target.py )/2)
                })
                .text(function(d){return d.weight});
            link.exit().remove();

            var node = this.variables.canvas.selectAll("g.node")
                .data(this.dataSet.nodes, function(d) {
                    return d.index;});
            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .call(this.variables.graphForceLayout.drag);
            nodeEnter.append("svg:circle")
                .attr("r", this.variables.nodeRadius)
                .attr("cx", function(d){return d.px})
                .attr("cy", function(d){return d.py})
                .attr("class","circle");
            nodeEnter.append("svg:text")
                .attr("class","nodeName")
                .text(function(d){return d.index});
            node.exit().remove();

            this.variables.graphForceLayout.on("tick", tick);
            var t = this;
            function tick(e) {
                link.selectAll('*')
                    .attr("x1", function(d){ return d.source.x; })
                    .attr("y1", function(d){ return d.source.y; })
                    .attr("x2", function(d){ return d.target.x; })
                    .attr("y2", function(d){ return d.target.y; });
                link.select('text')
                    .attr('x', function(d){
                        //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                        return Math.round((d.source.x + d.target.x )/2)
                    })
                    .attr('y', function(d){
                        //    найдем коордиаты середины отрезка, там и разместим лейблу с весом ребра
                        return Math.round((d.source.y + d.target.y )/2)
                    });
                node.selectAll('circle')
                    .attr("cx", function(d) { return d.x; })
                    .attr("cy", function(d) { return d.y; });
                node.select('text')
                    .attr("x", function(d){return (d.x +  20);})
                    .attr("y", function(d){ return d.y - 5 ;});
            }

            //при перерисовке графа обновляем матрицу смежности
            this.generateAdjacencyMatrix();

        };


        this.init = function(){

            //определеяем рабочую область канваса svg
            var svg = d3.select('body')
                .append('svg')
                .attr('height', this.variables.documentHeight)
                .attr('width', this.variables.documentWidth);

            this.variables.canvas = svg;

            var force = d3.layout.force()
                .distance(400)
                .charge(-50)
                .size([this.variables.documentWidth, this.variables.documentHeight])
                .nodes(this.dataSet.nodes)
                .links(this.dataSet.links)
                .start();

            this.variables.graphForceLayout = force;

            //инициализируем координаты  нод - пока для теста так
            //var k = 0, maxSubnet = 6, netRadius = 210;
            //for (var i = 0; i < this.dataSet.nodes.length; i++)
            //    if (this.dataSet.nodes[i].fixed){
            //        this.dataSet.nodes[i].x = this.variables.documentWidth / 2 + netRadius * Math.cos(2 * Math.PI * k / maxSubnet);
            //        this.dataSet.nodes[i].y = this.variables.documentHeight / 2 + netRadius * Math.sin(2 * Math.PI * k / maxSubnet);
            //        k++;
            //    }

            // только после того как определеили все параметри нод можем запускать инициализацию лейаута
            this.renderGraph();
        };

        this.startEditMode = function(){
            var lineAnimateDuratin = 1000,
                thisObj = this;
            var documentDimensions = {
                    height: this.variables.documentHeight,
                    width: this.variables.documentWidth
                },
                gridStep = this.variables.nodeRadius*2,
                gridDimensions = {
                    rows: Math.round(this.variables.documentHeight/gridStep),
                    cols: Math.round(this.variables.documentWidth/gridStep)
                },
                gridRowsDataSet = [],
                gridColsDataSet = [] ;

            var fillGridDataSet = function(step){
                gridRowsDataSet.push(step);
                return (step  < gridDimensions.rows * gridStep)? fillGridDataSet(step + gridStep):gridRowsDataSet
            };
            var fillColsDataSet = function(step){
                gridColsDataSet.push(step);
                return (step  < gridDimensions.cols * gridStep)? fillColsDataSet(step + gridStep):gridColsDataSet
            };
            fillGridDataSet(0);
            fillColsDataSet(0);

            var rows = this.variables.canvas.selectAll('.grid-row').data(gridRowsDataSet).enter().append('line');
            var cols = this.variables.canvas.selectAll('.grid-col').data(gridColsDataSet).enter().append('line');


            if (this.variables.editMode){
                    rows
                   .attr('class','grid-row')
                   .attr("x1", function(d){ return 0; })
                   .attr("y1", function(d){ return d + gridStep; })
                   //дополнительно для анимации
                   .attr("x2", function(d){ return 0; })
                   .attr("y2", function(d){ return d + gridStep; })
                   .transition()
                   .duration(lineAnimateDuratin)
                   .attr("x2", function(d){ return documentDimensions.width })
                   .attr("y2", function(d){ return d + gridStep })


                    cols
                   .attr('class','grid-col')
                   .attr("x1", function(d){ return d + gridStep; })
                   .attr("y1", function(d){ return 0; })
                   //дополнительно для анимации
                   .attr("x2", function(d){ return d + gridStep; })
                   .attr("y2", function(d){ return 0; })
                   .transition()
                   .duration(lineAnimateDuratin)
                   .attr("x2", function(d){ return d + gridStep })
                   .attr("y2", function(d){ return documentDimensions.height })
           }else{

                //    отключаем
                    this.variables.canvas.selectAll('.grid-row')
                   .transition()
                   .duration(lineAnimateDuratin)
                   .attr("x1", function(d){ return 0; })
                   .attr("y1", function(d){ return d - gridStep; })
                   .attr("x2", function(d){ return 0 })
                   .attr("y2", function(d){ return d - gridStep })
                    //.data([]).exit().remove();

                this.variables.canvas.selectAll('.grid-col')

                    .transition()
                   .duration(lineAnimateDuratin)
                   .attr("y1", function(d){ return d - gridStep; })
                   .attr("y1", function(d){ return 0; })
                   .attr("y2", function(d){ return d - gridStep })
                   .attr("y2", function(d){ return 0 })
                   //.data([]).exit().remove();

               setTimeout(function(){
                   thisObj.variables.canvas.selectAll('.grid-row').remove();
                   thisObj.variables.canvas.selectAll('.grid-col').remove();
               }, lineAnimateDuratin);




            }
            app.variables.editMode = ! app.variables.editMode;
        }

        //для дебага
        this.consoleLogMatrix = function(matrix){
            for (var i=0; i < matrix.length; i++){
                console.log(matrix[i].toString());
            }
        };
        // заполняет матрицу смежностти графа, на основе данных с d3
        this.generateAdjacencyMatrix = function(){
            //инициализируем массив
            for (var i = 0; i < this.dataSet.nodes.length; i++){
                this.dataSet.graphMatrix[i] = [];
                for (var j = 0 ; j < this.dataSet.nodes.length; j++){
                    this.dataSet.graphMatrix[i][j] = 0;
                }}
            //устанавливаем вес кажого из ребер в нужную ячейку
            for (var k = 0; k  < this.dataSet.nodes.length ; k ++){
                for (var l = 0; l < this.dataSet.links.length ; l++){
                    //console.log(k);
                    var graphMatrixIndex = this.dataSet.links[l].source.index;
                    if (this.dataSet.links[l].source.index == k){
                        this.dataSet.graphMatrix[graphMatrixIndex][this.dataSet.links[l].target.index] = this.dataSet.links[l].weight
                        this.dataSet.graphMatrix[this.dataSet.links[l].target.index][graphMatrixIndex] = this.dataSet.links[l].weight
                    }
                }
            }


            this.consoleLogMatrix(this.dataSet.graphMatrix);
        };

        //Функция поиска кратчайшего пути от заданой вершины к остальным вершинам графа (маршрутизация) по алгоритму Форда-Беллмана
        // start - вершина для которой производится поиск
        // nodesCount - колличство вершин графа
        // matrix - матрица смежности графа
        this.searchShortestPathes = function(start, nodesCount, matrix){

            //готовим массив к поиску TODO нужно ли это?
            //    var array = this.dataSet.nodes;
            //    var newVertex = array.splice(start,1);
            //    array.unshift(newVertex[0]);

            array = this.dataSet.nodes;
            console.log(array);

            var searchTable = [];
            // тут храним текущею и предидущую вершиныЮ для исключения их с проверки
            var  vertexArray = [];
            // сначала инициализируем таблицу поиска (всем кроме стартовой вершины - infinity)
            for (var k =0; k < nodesCount; k ++){
                searchTable[k] = Infinity
            }
            // вершину начала поиска обнуляем
            searchTable[start] = 0;
            vertexArray.push(start);

            // n-1 итераций согласно алгоритму
            for(var m=0; m < array.length -1 ; m++){
                for (var i=0; i < array.length; i++){
                    vertexArray.push(array[i].index);
                    var prevVertex =vertexArray[0];
                    var tempVertex =vertexArray[vertexArray.length-1];
                    console.log("prev vertex" + vertexArray[0]);
                    console.log("temp vertex" + tempVertex);
                    for (var j=0; j < array.length; j++){
                        if (matrix[tempVertex][j] > 0 &&  matrix[tempVertex][j] + searchTable[tempVertex] < searchTable[j]){
                            searchTable[j] = matrix[tempVertex][j] + searchTable[tempVertex];
                        }
                    }
                    console.log(searchTable);
                    console.log("===================");
                    vertexArray.shift();
                }
            }
        };
    };

    var app = new APP();
    app.init();
    //пробуем поиск
    app.generateAdjacencyMatrix();
    //app.searchShortestPathes(3, app.dataSet.nodes.length, app.dataSet.graphMatrix);
    //app.renderGraph();

    //app.removeLink(0,1);
    //app.renderGraph();

    //app.addLink(1,3, 1);

    //console.log("===============");

    //TODO перед пересчетом ми нимального пути постоянно перестраивать матрицу смежности!!!
    //app.searchShortestPathes(1, app.dataSet.nodes.length, app.dataSet.graphMatrix);

    //app.removeAllLinks();
    app.removeAllNodes();
    app.addNode(
        {"x":300, "y":250, "name": "A", "fixed": true}
    );
    app.addNode(
        {"x":500, "y":350, "name": "B", "fixed": true}
    );

    app.addNode(
        {"x":100, "y":250, "name": "C", "fixed": true}
    );
    app.addNode(
        {"x":400, "y":150, "name": "D", "fixed": true}
    );
    app.addLink(1,2, 1);
    app.addLink(0,3, 4);
    app.addLink(3,1, 2);
    app.addLink(0,2, 9);



    document.getElementById('test').onclick = function(){
       //app.addNode(
       //    {"x":500, "y":350, "name": "W", "fixed": true}
       //);
       // app.removeNode(0);


       // app.removeLink(0,1);

        //app.addLink(1,3, 1);





        //app.renderGraph();

        app.startEditMode();


    }

});